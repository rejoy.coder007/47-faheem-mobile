class Config {
  static String MyIpAddress = "http://192.168.0.104";
  static String url_login = (Config.MyIpAddress+"/aa_user_login").toString();
  static String url_view_random_packages = (Config.MyIpAddress+"/aa_apparel_user_view_package").toString();
  static String url_view_full_packages = (Config.MyIpAddress+"/aa_apparel_user_full_view_package").toString();
  static String url_carousel = (Config.MyIpAddress+"/ae_get_random_carousel_apparel").toString();
  static String url_book = (Config.MyIpAddress+"/ab_apparel_user_book_item").toString();
  static String url_cart = (Config.MyIpAddress+"/ab_apparel_user_cart_item").toString();
  static String url_show_book_by_id = (Config.MyIpAddress+"/ac_apparel_show_book_all_package").toString();
  static String url_show_cart_by_id = (Config.MyIpAddress+"/ac_apparel_show_cart_all_package").toString();


  static int Id = 0;
  static String user_name;
  static String email;
  static bool TEST = false;

  static int NUMBER_RANDOM_PRODUCT_HOME = 4;
  static int NUMBER_RANDOM_PRODUCT_HOME_ITEM_SIZE =8;
}